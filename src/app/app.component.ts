import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  posts = [
    {
      title: 'Mon premier post',
      content: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur delectus dolores incidunt iure nobis quam, tempora veritatis. Aliquam autem commodi, deleniti dignissimos earum exercitationem ipsum, maxime numquam provident repellendus, vero.',
      loveIts: 0,
      date: new Date()
    },
    {
      title: 'Mon deuxième post',
      content: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur delectus dolores incidunt iure nobis quam, tempora veritatis. Aliquam autem commodi, deleniti dignissimos earum exercitationem ipsum, maxime numquam provident repellendus, vero.',
      loveIts: 0,
      date: new Date()
    },
    {
      title: 'Encore un post',
      content: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur delectus dolores incidunt iure nobis quam, tempora veritatis. Aliquam autem commodi, deleniti dignissimos earum exercitationem ipsum, maxime numquam provident repellendus, vero.',
      loveIts: 0,
      date: new Date()
    }
  ];

}
