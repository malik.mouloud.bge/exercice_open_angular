import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-post-list-item',
  templateUrl: './post-list-item.component.html',
  styleUrls: ['./post-list-item.component.scss']
})
export class PostListItemComponent implements OnInit {


  dloveIts=0;
  loveIts=0;


  @Input() title: string;
  @Input() content: string;
  @Input() date: Date;
  @Input() loveIts: number;
  @Input() dloveIts: number;

  constructor() { }

  like(){
    this.loveIts +=1;

  }

  dislike(){
    this.dloveIts +=1;

  }


  ngOnInit() {
  }

}

